class LivrosDao {

    constructor(db) {
        this._db = db
    }

    listaLivros() {
        return new Promise((resolve, reject) => {
            this._db.all(
                'SELECT * FROM livros',
                (erro, resultado) => {
                    if (erro) return reject('Ocorreu um erro na busca!')
                    return resolve(resultado)
                })
        })

    }


    adiciona(livros) {
        return new Promise((resolve, reject) => {
            this._db.run(`
                INSERT INTO livros(
                    titulo,
                    preco,
                    descricao
                ) VALUES (?, ? ,?)
            `, [
                    livros.titulo,
                    livros.preco,
                    livros.descricao
                ],
                function (err) {
                    if (err) {
                        console.log(err);
                        return reject(`Não foi possível adicionar o livro!`);
                    }
                    resolve();
                }
            )
        })
    }

    buscarPorId(id) {

        return new Promise((resolve, reject) => {
            this._db.get(
                `
             SELECT * from livros where id = ?  
            `
                , [id],
                (erro, livros) => {
                    if (erro) {
                        return reject('Erro ao executar a busca');
                    }
                    return resolve(livros);
                })
        })
    }

    atualiza(livros) {

        return new Promise((resolve, reject) => {
            this._db.run(
                `
                UPDATE livros SET 
                titulo = ?,
                preco = ?,
                descricao = ?,
                WHERE id = ?        
                `, [
                    livros.titulo,
                    livros.preco,
                    livros.descricao,
                    livros.id
                ],
                erro => {
                    if (erro) {
                        return reject(`Erro ao atualizar os dados`)
                    }
                    resolve()
                }
            )
        })
    }

    remove(id) {

        return new Promise((resolve, reject) =>{

            this._db.get(
                `
                DELETE FROM livros WHERE id = ?
                `,[id],
                (erro) => {
                    if(erro){
                        return reject(`Erro ao deletar `)
                    }
                    return resolve()
                }
            )
        })
    }


}

module.exports = LivrosDao;