// Compiled using marko@4.14.15 - DO NOT EDIT
"use strict";

var marko_template = module.exports = require("marko/src/html").t(__filename),
    marko_componentType = "/teste$1.0.0/src/app/view/livros/lista/listagem.marko",
    components_helpers = require("marko/src/components/helpers"),
    marko_renderer = components_helpers.r,
    marko_defineComponent = components_helpers.c,
    marko_helpers = require("marko/src/runtime/html/helpers"),
    marko_loadTag = marko_helpers.t,
    component_globals_tag = marko_loadTag(require("marko/src/components/taglib/component-globals-tag")),
    marko_forEach = marko_helpers.f,
    marko_escapeXml = marko_helpers.x,
    marko_attr = marko_helpers.a,
    init_components_tag = marko_loadTag(require("marko/src/components/taglib/init-components-tag")),
    await_reorderer_tag = marko_loadTag(require("marko/src/taglibs/async/await-reorderer-tag"));

function render(input, out, __component, component, state) {
  var data = input;

  out.w("<html><head><title>Teste NodeJs</title></head><body>");

  component_globals_tag({}, out);

  out.w("<h1><b>Livros</b></h1><table id=\"livros\"><thead><tr><th>ID</th><th>Titulos</th><th>Preco</th><th>Editar</th><th>Remover</th></tr></thead><tbody>");

  var for__15 = 0;

  marko_forEach(data.livros, function(livros) {
    var keyscope__16 = "[" + ((for__15++) + "]");

    out.w("<tr" +
      marko_attr("id", "livros_" + livros.id) +
      "><td>" +
      marko_escapeXml(livros.id) +
      "</td><td>" +
      marko_escapeXml(livros.titulo) +
      "</td><td>" +
      marko_escapeXml(livros.preco) +
      "</td><td>" +
      marko_escapeXml(livros.descricao) +
      "</td><td><a" +
      marko_attr("href", "/livros/form/" + livros.id) +
      ">Editar</a></td><td><a href=\"#\"" +
      marko_attr("data-ref", "" + livros.id) +
      " data-type=\"remocao\">Remover</a></td></tr>");
  });

  out.w("</tbody></table><script src=\"/static/js/removerLivro.js\"> </script>");

  init_components_tag({}, out);

  await_reorderer_tag({}, out, __component, "27");

  out.w("</body></html>");
}

marko_template._ = marko_renderer(render, {
    ___implicit: true,
    ___type: marko_componentType
  });

marko_template.Component = marko_defineComponent({}, marko_template._);

marko_template.meta = {
    id: "/teste$1.0.0/src/app/view/livros/lista/listagem.marko",
    tags: [
      "marko/src/components/taglib/component-globals-tag",
      "marko/src/components/taglib/init-components-tag",
      "marko/src/taglibs/async/await-reorderer-tag"
    ]
  };
