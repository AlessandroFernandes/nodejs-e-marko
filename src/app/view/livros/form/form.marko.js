// Compiled using marko@4.14.15 - DO NOT EDIT
"use strict";

var marko_template = module.exports = require("marko/src/html").t(__filename),
    marko_componentType = "/teste$1.0.0/src/app/view/livros/form/form.marko",
    components_helpers = require("marko/src/components/helpers"),
    marko_renderer = components_helpers.r,
    marko_defineComponent = components_helpers.c,
    marko_helpers = require("marko/src/runtime/html/helpers"),
    marko_loadTag = marko_helpers.t,
    component_globals_tag = marko_loadTag(require("marko/src/components/taglib/component-globals-tag")),
    marko_attr = marko_helpers.a,
    init_components_tag = marko_loadTag(require("marko/src/components/taglib/init-components-tag")),
    await_reorderer_tag = marko_loadTag(require("marko/src/taglibs/async/await-reorderer-tag"));

function render(input, out, __component, component, state) {
  var data = input;

  out.w("<html><body>");

  component_globals_tag({}, out);

  out.w("<h1> Cadastro de Livros </h1><form action=\"/livros\" method=\"post\">");

  if (data.livros.id) {
    out.w("<div><input type=\"hidden\" name=\"_method\" value=\"PUT\"><input type=\"hidden\" id=\"id\" name=\"id\"" +
      marko_attr("value", "" + data.livros.id) +
      "></div>");
  }

  out.w("<div><label for=\"titulo\">Título</label><input type=\"text\" id=\"titulo\" name=\"titulo\"" +
    marko_attr("value", "" + data.livros.titulo) +
    " placeholder=\"Coloque o titulo de seu livro\"></div><div><label for=\"preco\">Preço</label><input type=\"text\" id=\"preco\" name=\"preco\"" +
    marko_attr("value", "" + data.livros.preco) +
    " placeholder=\"Digite o preco\"></div><div><label for=\"descricao\">Descrição</label><textarea cols=\"20\" rows=\"10\" id=\"descricao\"" +
    marko_attr("value", "" + data.livros.descricao) +
    " name=\"descricao\" placeholder=\"Digite uma descricao\"></textarea></div><input type=\"submit\" value=\"Salvar\"></form>");

  init_components_tag({}, out);

  await_reorderer_tag({}, out, __component, "17");

  out.w("</body></html>");
}

marko_template._ = marko_renderer(render, {
    ___implicit: true,
    ___type: marko_componentType
  });

marko_template.Component = marko_defineComponent({}, marko_template._);

marko_template.meta = {
    id: "/teste$1.0.0/src/app/view/livros/form/form.marko",
    tags: [
      "marko/src/components/taglib/component-globals-tag",
      "marko/src/components/taglib/init-components-tag",
      "marko/src/taglibs/async/await-reorderer-tag"
    ]
  };
