const LivrosDao = require('../infra/LivrosDao');
const db = require('../config/database');
module.exports = (app) => {

    app.get('/', function (req, resp) {
        resp.send(`<html>
                    <head>
                        <title>Teste NodeJs</title>
                    </head>
                    <body>
                        <h1><b>Servidor NodeJs</h1>
                    </body>
                </html>`)
    })

    app.get('/livros', function (req, resp) {
        livroDao = new LivrosDao(db);
        livroDao.listaLivros()
            .then(livros => resp.marko(
                require('../app/view/livros/lista/listagem.marko'),
                {
                    livros: livros
                }
            ))
            .catch(erro => console.log(erro))
    })

    app.get('/livros/form', function(req, resp){
        resp.marko(require('../app/view/livros/form/form.marko'), {livros : {}})
    })

    app.post('/livros', function(req, resp){
        console.log(req.body);
        const livroDao = new LivrosDao(db);
        livroDao.adiciona(req.body)
                .then(resp.redirect('/livros'))
                .catch(erro => console.log(erro));
    })

    app.put('/livros', function(req, resp){
        console.log(req.body);
        const livroDao = new LivrosDao(db);
        livroDao.atualiza(req.body)
                .then(resp.redirect('/livros'))
                .catch(erro => console.log(erro));
    })

    app.get('/livros/form/:id',  function(req, resp){

        const id = req.params.id;
        const livroDao = new LivrosDao(db);

        livroDao.buscarPorId(id)
                .then(livros => 
                    resp.marko(require('./view/livros/form/form.marko'),
                        {livros : livros}
                    )
                )
                .catch(erro => console.log(erro))
    });

    app.delete('/livros:id', function(req, resp){
        const id = req.params.id;
        const livroDao = new LivrosDao(db);
        livroDao.remove(id)
                .then(() => resp.status(200).end())
                .catch(erro => console.log(erro))
    })


}
